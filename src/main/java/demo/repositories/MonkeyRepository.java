package demo.repositories;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import demo.models.Monkey;

@Repository
public interface MonkeyRepository extends JpaRepository<Monkey, Long>{
	@Query("SELECT a FROM Monkey a WHERE a.playerNumber=:phone")
	public ArrayList<Monkey> findByPhone(@Param("phone") String phone);
}
