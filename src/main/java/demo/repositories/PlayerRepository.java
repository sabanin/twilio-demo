package demo.repositories;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import demo.models.Player;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long>{
	@Query("SELECT a FROM Player a WHERE a.phone=:phone")
	public ArrayList<Player> findByPhone(@Param("phone") String phone);
}
