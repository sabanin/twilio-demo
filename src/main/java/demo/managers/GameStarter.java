package demo.managers;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Call;

public class GameStarter {
	
	public static final String ACCOUNT_SID = "ACae0e4ebbe6628327976fd616e466b870";
	public static final String AUTH_TOKEN = "00fde28d794c16c12fb50f4bac6f9bfd";
	
	public void start(String phone) throws TwilioRestException{
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
	    
	    // Build a filter for the CallList
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("Url", "http://twiliodnddemo.herokuapp.com/hello"));
	    params.add(new BasicNameValuePair("To", phone));
	    params.add(new BasicNameValuePair("From", "+3728801456"));
	    params.add(new BasicNameValuePair("Method", "GET"));
	     
	    CallFactory callFactory = client.getAccount().getCallFactory();
	    Call call = callFactory.create(params);
	    System.out.println(call.getSid());
	}
}
