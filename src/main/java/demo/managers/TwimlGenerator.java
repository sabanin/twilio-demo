package demo.managers;

import java.util.ArrayList;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twilio.sdk.verbs.Gather;
import com.twilio.sdk.verbs.Play;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.TwiMLResponse;

import demo.models.Cave;
import demo.models.CaveCell;
import demo.models.Monkey;
import demo.models.Player;
import demo.repositories.MonkeyRepository;
import demo.repositories.PlayerRepository;

@Service
public class TwimlGenerator {
	
	@Autowired
	PlayerRepository repo;
	@Autowired
	MonkeyRepository monkeys;

	public TwiMLResponse generate(Player player) throws TwiMLException{
		TwiMLResponse twiml = new TwiMLResponse();
		Integer currentStage = player.getCurrentStage();
		if (currentStage == 0) 
		{
			Say sayWelcome = new Say("Hello, "+player.getName()+"! Welcome to the incredible Dungeon and Monkeys game! Let's begin! ");
			twiml.append(sayWelcome);
		}
		CaveCell currentCell = new Cave().getCaveCells().get(currentStage+1);
		if (!currentCell.getIntroMusic().equals("")) twiml.append(new Play(currentCell.getIntroMusic()));
		Say sayDescription = new Say(currentCell.getDescriprion());
		twiml.append(sayDescription);
		Say sayActions = new Say(currentCell.getActions() + ". You can exit game if you press #. ");
		
		Gather gather = new Gather();
		gather.setAction("/"+currentCell.getActionsType());
        gather.setNumDigits(1);
        gather.setMethod("GET");
        gather.setFinishOnKey("#");
        gather.append(sayActions);
        
        twiml.append(gather);
		return twiml;
	}
	
	public TwiMLResponse generateMonkeyMove(Player player, TwiMLResponse twiml) throws TwiMLException{
		Monkey monkey = monkeys.findByPhone(player.getPhone()).get(0);
		Say sayAttack = new Say(monkey.getAttackDescription());
		twiml.append(sayAttack);
		twiml.append(new Play("http://s1download-universal-soundbank.com/mp3/sounds/3501.mp3"));
		Random rnd = new Random();
		Double attackDice = rnd.nextDouble();
		if (attackDice <= monkey.getChance()){
			player.setHp(player.getHp()-monkey.getAttack());
			Say saySuccess = new Say(" Success! Monkey hit you. And you have got "+monkey.getAttack()+" points of damage.");
			twiml.append(saySuccess);
		} else {
			Say saySuccess = new Say(" Monkeys slip on babana peel! ");
			twiml.append(saySuccess);
		}
		repo.saveAndFlush(player);
		Gather gather = new Gather();
		if (player.getHp()>0){
			gather.setAction("/fightstep");
	        gather.setNumDigits(1);
	        gather.setMethod("GET");
	        gather.setFinishOnKey("#");
	        gather.append(new Say("To cast fireball press 1. To cast hypnosis press 2. To leave game press #"));	       
		} else {
			gather.setAction("/again");
	        gather.setNumDigits(1);
	        gather.setMethod("GET");
	        gather.setFinishOnKey("#");
	        gather.append(new Say("Crazy monkey killed you. You dead. To start game again press 1.  You can exit game if you press #."));	        
		}
        twiml.append(gather);
		return twiml;
	}
	
	public TwiMLResponse generateFireballAttack(Player player) throws TwiMLException{
		TwiMLResponse twiml = new TwiMLResponse();
		Monkey monkey = monkeys.findByPhone(player.getPhone()).get(0);
		Say sayAttackDescription = new Say ("You rise your hand and cast huge fireball! ");
		twiml.append(sayAttackDescription);
		twiml.append(new Play("http://s1download-universal-soundbank.com/mp3/sounds/615.mp3"));
		Random rnd = new Random();
		Double attackDice = rnd.nextDouble();
		if (attackDice <= player.getChance()){
			monkey.setHp(monkey.getHp()-player.getAttack());
			Say saySuccess = new Say(" Success! Monkeys for is burning. Monkey get "+player.getAttack()+" points of damage.");
			twiml.append(saySuccess);
		} else {
			Say saySuccess = new Say(" You slip on babana peel! Fireball flyed away! ");
			twiml.append(saySuccess);
		}
		if (monkey.getHp()<=0){
			Say sayVictory = new Say("Monkey is dead! You  have got experience and ancient sword! Your attack increase by 1. ");
			player.setAttack(player.getAttack()+1);
//			player.setCurrentStage(player.getCurrentStage()+1);
			twiml.append(new Play("http://s1download-universal-soundbank.com/mp3/sounds/1302.mp3"));
			twiml.append(sayVictory);
			
			Gather gather = new Gather();
			gather.setAction("/move");
	        gather.setNumDigits(1);
	        gather.setMethod("GET");
	        gather.setFinishOnKey("#");
	        gather.append(new Say("Press 1 to continue. "));
	        
	        twiml.append(gather);
		}
		repo.saveAndFlush(player);
		monkeys.saveAndFlush(monkey);
		return twiml;
	}
	
	public TwiMLResponse generateHypnoseAttack(Player player) throws TwiMLException{
		TwiMLResponse twiml = new TwiMLResponse();
		Monkey monkey = monkeys.findByPhone(player.getPhone()).get(0);
		Say sayAttackDescription = new Say ("You look right into Monkeys eyes. You see madness. Your hypnose attack make Mokey more angry and less accurate");
		monkey.setAttack(monkey.getAttack()+1);
		monkey.setChance(monkey.getChance()- 0.1 );
		
		twiml.append(sayAttackDescription);
		monkeys.saveAndFlush(monkey);
		repo.saveAndFlush(player);
		return twiml;
	}
}
