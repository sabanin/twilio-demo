package demo.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.twilio.sdk.TwilioRestException;

import demo.managers.GameStarter;
import demo.models.Monkey;
import demo.models.Player;
import demo.repositories.MonkeyRepository;
import demo.repositories.PlayerRepository;

@Controller
public class PlayerController {

	@Autowired
	PlayerRepository repo;
	@Autowired
	MonkeyRepository monkeys;
	
	@RequestMapping("/")
	public String getIndex(Model model){
		model.addAttribute("player", new Player());
		return "player/new";
	}
	
	@RequestMapping(value = "/player/create", method = RequestMethod.POST)
	public String savePlayer(Player player) throws TwilioRestException{
		ArrayList<Player> players = repo.findByPhone(player.getPhone());
		repo.delete(players);
		repo.saveAndFlush(player);
		GameStarter gameStarter = new GameStarter();
		gameStarter.start(player.getPhone());
		return "player/create";
	}
	
	@RequestMapping(value = "/player/continue", method = RequestMethod.POST)
	public String continuePlayer(Player player) throws TwilioRestException{
		GameStarter gameStarter = new GameStarter();
		gameStarter.start(player.getPhone());
		return "player/create";
	}
}
