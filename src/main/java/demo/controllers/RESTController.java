package demo.controllers;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.sdk.verbs.Play;
import com.twilio.sdk.verbs.TwiMLResponse;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.Gather;

import demo.managers.TwimlGenerator;
import demo.models.Monkey;
import demo.models.Player;
import demo.repositories.MonkeyRepository;
import demo.repositories.PlayerRepository;


@RestController
public class RESTController {
	
	@Resource private HttpServletRequest request;
	@Resource private HttpServletResponse response;
	
	@Autowired
	TwimlGenerator generator;
	@Autowired
	PlayerRepository repo;
	@Autowired
	MonkeyRepository monkeys;
	
	@RequestMapping(method = RequestMethod.GET, value = "/hello")
	public void getHello() throws IOException, TwiMLException{
		String playerNumber = request.getParameter("To");
        Player player = repo.findByPhone(playerNumber).get(0);
        TwiMLResponse twiml = generator.generate(player);
        response.setContentType("application/xml");
        response.getWriter().print(twiml.toXML());
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/move")
	public void processMoveGather() throws IOException{
		String digits = request.getParameter("Digits");
		String playerNumber = request.getParameter("To");
		Player player = repo.findByPhone(playerNumber).get(0);
        if (digits != null && digits.equals("1")) {
        	player.setCurrentStage(player.getCurrentStage()+1);
        	repo.saveAndFlush(player);
        } 
//        response.setContentType("application/xml");
//        response.getWriter().print(twiml.toXML());
        response.sendRedirect("/hello");
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/jump")
	public void processJumpGather() throws IOException, TwiMLException{
		String digits = request.getParameter("Digits");
		String playerNumber = request.getParameter("To");
		Player player = repo.findByPhone(playerNumber).get(0);
        if (digits != null && digits.equals("1")) {
        	player.setCurrentStage(player.getCurrentStage()+2);
        	repo.saveAndFlush(player);
        }
        if (digits != null && digits.equals("2")) {
        	player.setCurrentStage(player.getCurrentStage()+1);
        	repo.saveAndFlush(player);
        }
//        response.setContentType("application/xml");
//        response.getWriter().print(twiml.toXML());
        response.sendRedirect("/hello");
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/again")
	public void processAgainGather() throws IOException, TwiMLException{
		String digits = request.getParameter("Digits");
		String playerNumber = request.getParameter("To");
		Player player = repo.findByPhone(playerNumber).get(0);
        if (digits != null && digits.equals("1")) {
        	player.setCurrentStage(0);
        	player.setHp(10);
        	repo.saveAndFlush(player);
        }
        response.sendRedirect("/hello");
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/battle")
	public void processBattleGather() throws IOException, TwiMLException{
		String digits = request.getParameter("Digits");
		String playerNumber = request.getParameter("To");
        if (digits != null && digits.equals("1")) {
        	Monkey monkey = new Monkey();
        	monkey.setPlayerNumber(playerNumber);
        	monkeys.save(monkey);
        	response.sendRedirect("/infight");
        	return;
        }
        response.sendRedirect("/hello");
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/fightstep")
	public void processFightStepGather() throws IOException, TwiMLException{
		String digits = request.getParameter("Digits");
		String playerNumber = request.getParameter("To");
		Player player = repo.findByPhone(playerNumber).get(0);
        if (digits != null && digits.equals("1")) {
        	TwiMLResponse  twiml = generator.generateFireballAttack(player);
        	
            player = repo.findByPhone(playerNumber).get(0);
            Monkey monkey = monkeys.findByPhone(playerNumber).get(0);
        	if (monkey.getHp()<0)
        	{
        		response.sendRedirect("/hello");
        		return;
        	}
        	twiml = generator.generateMonkeyMove(player, twiml);
        	response.setContentType("application/xml");
            response.getWriter().print(twiml.toXML());
            
        	return;
        }
        
        if (digits != null && digits.equals("2")) {
        	TwiMLResponse  twiml = generator.generateHypnoseAttack(player);
        	twiml = generator.generateMonkeyMove(player, twiml);
        	response.setContentType("application/xml");
            response.getWriter().print(twiml.toXML());
        	return;
        }
        response.sendRedirect("/hello");
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/infight")
	public void processFight() throws IOException, TwiMLException{
		String playerNumber = request.getParameter("To");
        Player player = repo.findByPhone(playerNumber).get(0);
        TwiMLResponse twiml = generator.generateMonkeyMove(player, new TwiMLResponse());
        response.setContentType("application/xml");
        response.getWriter().print(twiml.toXML());
	}
}


