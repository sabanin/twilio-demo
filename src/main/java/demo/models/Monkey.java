package demo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@XmlRootElement
@Data
public class Monkey {
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	Integer hp = 10;
	Integer attack = 3;
	Double chance = 0.5;
	String attackDescription = " Crazy Monkey trying to reach you with sword!";
	
	String playerNumber;
}
