package demo.models;

import lombok.Data;

@Data
public class CaveCell {
	String descriprion;
	String actions;
	String actionsType;
	String introMusic = "";
	
	public CaveCell(String descriprion, String actions, String actionsType){
		this.descriprion = descriprion;
		this.actions = actions;
		this.actionsType = actionsType;
	}
}
