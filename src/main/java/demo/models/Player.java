package demo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@XmlRootElement
@Data
public class Player {
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	String name;
	String phone;
	Integer currentStage = 0;
	
	//Game parameters
	Integer hp = 10;
	Integer attack = 5;
	Double chance = 0.7;
	
	/*public Player(String name, String phone){
		this.name = name;
		this.phone = phone;
	}*/
}
