package demo.models;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Cave {
	List<CaveCell> caveCells = new ArrayList<CaveCell>();
	
	public Cave(){
		caveCells.add(new CaveCell("", "", "move"));
		caveCells.add(new CaveCell("You find yourself on the cold ground in dark and unpleasant cave. The water drops from the ceiling. ", "Press 1 to move forward. ", "move"));
		caveCells.get(1).setIntroMusic("http://s1download-universal-soundbank.com/mp3/sounds/1073.mp3");
		caveCells.add(new CaveCell("Wind is blowing into your face. You are freezing. The smells of bananas in the air. ", "Press 1 to move forward. ", "move"));
		caveCells.get(2).setIntroMusic("http://s1download-universal-soundbank.com/mp3/sounds/715.mp3");
		caveCells.add(new CaveCell("The eyes got used to the dark. You see pit in front of you. What are you going to do? ", "Press 1 to jump over the pit. Press 2 to go into the hole. ", "jump"));
		caveCells.get(3).setIntroMusic("http://s1download-universal-soundbank.com/mp3/sounds/712.mp3");
		caveCells.add(new CaveCell("Such a pitty. You died. ", "Press 1 if you want to start again. ", "again"));
		caveCells.get(4).setIntroMusic("http://s1download-universal-soundbank.com/mp3/sounds/22019.mp3");
		caveCells.add(new CaveCell("Aggressive monkey seems to stand in your way. It hold ancient sword and ready for battle", "To start battle press 1. ", "battle"));
		caveCells.get(5).setIntroMusic("http://s1download-universal-soundbank.com/mp3/sounds/1296.mp3");
		caveCells.add(new CaveCell("You see the light in the end of the cave. Congratulations! You won! ", "Press 1 if you want to play game again. ", "again"));
		caveCells.get(6).setIntroMusic("http://s1download-universal-soundbank.com/mp3/sounds/18045.mp3");
		
	}
}
