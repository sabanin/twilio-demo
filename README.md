Application is simple quest game where GameMaster (played by Twilio) describes location and propose actions to the player.
Player choose one of the action with his phone and game continue. 

* PlayerController responsible for creation of user, or starting game for existing one.
* RESTController responsible for mapping of requests that application send to itself (for each Gather, for example, different methods created. Each Gather method responsible for set of actions Player could do in exact situation).
* TwimlGenerator responsible for main logic of application. It formed TwimlResponse based on user actions and call sufficient url for user choises.   
* GameStarter make initial call to player.

I hope you play and have fun with game "Dungeon and Monkeys"!

AccountSID
ACae0e4ebbe6628327976fd616e466b870

Test AccountSID
ACd561ded2fb679620203c37d9577133dc